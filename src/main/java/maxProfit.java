/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.lang.Integer;
/**
 *
 * @author ADMIN
 */
public class maxProfit {
    public static void main(String[] args) {
        int[] n = {2,3,5,1,8,4,7,4,5,7,56,46,43,36,};
        int prices = maxProfit(n);
        System.out.println(prices);
        
    }
    public static int maxProfit(int[] prices) {
        int minPriceSoFar = Integer.MAX_VALUE, maxProfitSoFar = 0;
        
        for (int i = 0; i < prices.length; i++) {
            if (minPriceSoFar > prices[i]) {
                minPriceSoFar = prices[i];
            } else {
                maxProfitSoFar = Math.max(maxProfitSoFar, prices[i] - minPriceSoFar);
            }
        }
        
        return maxProfitSoFar;
    }
}
